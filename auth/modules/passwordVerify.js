exports.passwordVerify = function(password) {
    let lowerCaseLetters = /[a-z]/g;
    let upperCaseLetters = /[A-Z]/g;
    let numbers = /[0-9]/g;
    if ( password.match(lowerCaseLetters) &&
        password.match(upperCaseLetters) &&
        password.match(numbers) &&
        password.length >= 8 ) {
            return true;
    } else {
        return false;
    };
};