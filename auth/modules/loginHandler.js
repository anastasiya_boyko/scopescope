const queryToHasura = require('./queryHasuraFindUser');
const customJWT = require('./generateJWT');

exports.loginHandler = async function(login, password, res) {
    const hasuraRes = queryToHasura.findUser(login, password);
    await hasuraRes.then((data) => {
        if(data.users.length > 0) {
            res.send({
                accessToken: customJWT.generateJWT(login, password, data.users[0].id),
                userID: data.users[0].id
            })
        } else {
            res.send({});
        }
    });
}