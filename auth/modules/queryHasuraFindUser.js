const fetch = require('cross-fetch');
require('dotenv').config({path: './.env'});

exports.findUser  = async function (login, password) {       
    const hasuraResponse = await fetch(`${process.env.HASURA_FIND_USER_ENDPOINT}/${login}/${password}`, {
        headers: {
            'x-hasura-admin-secret': process.env.HASURA_GRAPHQL_ADMIN_SECRET
        }
    });
    return await hasuraResponse.json(); 
};