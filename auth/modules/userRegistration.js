const verify = require('./passwordVerify');
const fetch = require('cross-fetch');
require('dotenv').config({path: './.env'});

exports.userRegistration = function(login, password, res) {
    if( verify.passwordVerify(password) ) {    
        fetch(`${process.env.HASURA_CREATE_USER_ENDPOINT}/${login}/${password}`, {
            method: 'post',
            headers: {
                'x-hasura-admin-secret': process.env.HASURA_GRAPHQL_ADMIN_SECRET
            }
        });
        res.status(200);
        res.end();
    } else {
        res.status(205);
        res.end();
    }
}