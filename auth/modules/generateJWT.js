const jwt = require('jsonwebtoken');
require('dotenv').config({path: './.env'});

exports.generateJWT = function (login, password, id) {
    const payload = {
        "uid": id,
        "login": login,
        "password": password,
        "https://hasura.io/jwt/claims": { 
            "x-hasura-allowed-roles": ['user'],
            "x-hasura-default-role": 'user',
        }
    }
    return jwt.sign(payload, process.env.HASURA_GRAPHQL_JWT_SECRET, {
        algorithm: "HS256"
    })
};