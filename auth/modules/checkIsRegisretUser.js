require('dotenv').config({path: './.env'});
const registration = require('./userRegistration');
const fetch = require('cross-fetch');

exports.isUserAlreadyRegister = function(login, password, res) {
    fetch(`${process.env.HASURA_FIND_USER_ENDPOINT}/${login}/${password}`, {
        headers: {
            'x-hasura-admin-secret': process.env.HASURA_GRAPHQL_ADMIN_SECRET
        }
    })
    .then( function(response) {
        response.text().then(function(userFromDB) {
            if (JSON.parse(userFromDB).users.length != 0) {
                res.status(202);
                res.end();
            } else {
                registration.userRegistration(login, password, res);
            }           
        })
    });
}