const express = require('express');
const app = express();
const port = 4000;
const login = require('./modules/loginHandler');
const register = require('./modules/registerHandler');
const { json } = require('express');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', true);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);  
    next();
});

app.get('/register', (req, res) => {
    register.registerHandler(req.query.login, req.query.password, res);  
});

app.get('/login', async function(req, res) {
    login.loginHandler(req.query.login, req.query.password, res);
});
  
app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
});

