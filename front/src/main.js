import Vue from 'vue';
import App from './App.vue';
import router from './router/index';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import VueApollo from 'vue-apollo';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
require('dotenv').config({path: './.env'});

Vue.config.productionTip = false;

const httpLink = new HttpLink({
  uri: process.env.VUE_APP_HTTP_URI
});

const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache({
    addTypename: false
  }),
  connectToDevTools: true,
});

Vue.use(VueApollo);

const vuetifyOptions = {  };
Vue.use(Vuetify);

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});

new Vue({
  el: '#app',
  apolloProvider,
  router: router,
  vuetify: new Vuetify( vuetifyOptions ),
  render: h => h(App),
}).$mount('#app')
