import gql from 'graphql-tag';

export const ADD_USER = gql`
    mutation userRegister(
        $login: String!
        $password: String!
    ) {
        insert_users(
            objects: [
                {
                    login: $login,
                    password: $password
                }
            ]
        ) {
            returning {
                id
                login
                password
            }
        }
    }
`;