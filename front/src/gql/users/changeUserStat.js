import gql from 'graphql-tag';

export const UPDATE_USER = gql`
    mutation changeUserStatus($id: Int!, $isOnline: Boolean!) {
        update_users(where: {id: {_eq: $id }} _set: {isOnline: $isOnline}) {
            affected_rows
        }
    }
`;