import gql from 'graphql-tag';

export const REMOVE_COLUMN = gql`
    mutation removeColumn (
        $id: Int!
    ) {
        delete_columns( where: {id: {_eq: $id}}) {
            affected_rows
        }
    }
`;