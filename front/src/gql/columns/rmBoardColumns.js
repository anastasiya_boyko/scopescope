import gql from 'graphql-tag';

export const REMOVE_BOARD_COLUMNS = gql`
    mutation removeColumns (
        $id: Int!
    ) {
        delete_columns( where: {board_id: {_eq: $id}}) {
            affected_rows
        }
    }
`;