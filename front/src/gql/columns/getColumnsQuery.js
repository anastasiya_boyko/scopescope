import gql from 'graphql-tag';

export const GET_COLUMNS = gql`
    query getColumns {
        columns {
            id
            title
            board_id
            position
        }
    }
`;