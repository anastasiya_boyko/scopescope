import gql from 'graphql-tag';

export const INSERT_COLUMNS = gql`
    mutation insertColumns(
        # $id: Int,
        # $name: String,
        # $description: String,
        # $column_id: Int,
        # $dedline: Date,
        # $position: Int,
        # $status: String,
        # $executors: Int
        $array: [columns_insert_input!]!
    ) {
        insert_columns(objects: $array) {
            affected_rows
        }
    }
`;