import gql from 'graphql-tag';

export const ADD_COLUMN = gql`
    mutation addColumn (
        $board_id: Int!
        $title: String!
    ) {
        insert_columns(
            objects: [
                {
                    board_id: $board_id,
                    title: $title
                }
            ]
        ) {
            returning {
                id
                title
                board_id
            }
        }
    }
`;