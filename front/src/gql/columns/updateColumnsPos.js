import gql from 'graphql-tag';

export const  UPDATE_COLUMNS_POS = gql`
    mutation updateColumnsPos (
        $newArray: [columns_insert_input!]!
    ) {
        insert_columns(objects: $newArray, 
            on_conflict: {
                constraint: columns_pkey,
                update_columns: [position]
            }) {
                affected_rows
            }            
    }
`;