import gql from 'graphql-tag';

export const GET_BOARD_MEMBERS = gql`
    query getBoardMembers(
        $id: Int!
    ) {
        boards(  where: { id: { _eq: $id }} ) {
            board_members {
                members_user {
                    id
                    login
                }
            }
        }
    }
`;