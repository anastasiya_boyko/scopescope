import gql from 'graphql-tag';

export const ADD_MEMBER = gql`
    mutation addMember(
        $user_id: Int!
        $board_id: Int!
    ) {
        insert_user_to_board(
            objects: [
                {
                    user_id: $user_id,
                    board_id: $board_id
                }
            ]
        ) {
            returning {
                id
                user_id
                board_id
            }
        }
    }
`;