import gql from 'graphql-tag';

export const INVITE_USER = gql`
    mutation inviteUserToBoard(
        $user_id: Int!
        $board_id: Int!
    ) {
        insert_user_to_board(
            objects: {
                    user_id: $user_id,
                    board_id: $board_id
                }) 
            {
            affected_rows
        }
    }
`;