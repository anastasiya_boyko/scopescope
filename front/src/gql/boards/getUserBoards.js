import gql from 'graphql-tag';

export const GET_USER_BOARDS = gql`
    query getUserBoards(
        $user_id: Int!
    ) {
        boards(where: {board_creator: {id: {_eq: $user_id}}} ) {
            id
            title
        }
        user_to_board(where: {user_id: { _eq: $user_id }} ) {
            board_id
            board_title
        }
    }
`;