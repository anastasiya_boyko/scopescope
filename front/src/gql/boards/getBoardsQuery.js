import gql from 'graphql-tag';

export const GET_BOARDS = gql`
    query getBoards {
        boards {
            id
            title
            board_creator {
                id
                login
            }
        }
    }
`;