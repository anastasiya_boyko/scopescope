import gql from 'graphql-tag';

export const ADD_BOARD = gql`
    mutation addBoard(
        $title: String!
        $creator: Int!
    ) {
        insert_boards(
            objects: [
                {
                    title: $title,
                    creator: $creator,
                }
            ]
        ) {
            returning {
                id
                title
                creator
            }
        }
    }
`;