import gql from 'graphql-tag';

export const  UPDATE_TASKS_POS = gql`
    mutation updateTasksPos (
        $newArray: [tasks_insert_input!]!
    ) {
        insert_tasks(objects: $newArray, 
            on_conflict: {
                constraint: tasks_pkey,
                update_columns: [position, column_id]
            }) {
                affected_rows
            }            
    }
`;