import gql from 'graphql-tag';

export const DELETE_TASKS = gql`
    mutation deleteTasks( $column_id: Int!) {
        delete_tasks(where: {column_id: {_eq: $column_id}}) {
            affected_rows
        }
    }
`;