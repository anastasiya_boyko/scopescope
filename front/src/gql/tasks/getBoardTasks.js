import gql from 'graphql-tag';

export const GET_BOARD_TASKS = gql`
    query getTasks( $board_id: Int!) {
        tasks(where: {board_id: {_eq: $board_id}}) {
            name
            description
            executors
            dedline
            status
            column_id
            position
            priority
        }
    }
`;