import gql from 'graphql-tag';

export const GET_TASKS = gql`
    query getTasks( $column_id: Int!) {
        tasks(where: {column_id: {_eq: $column_id}}) {
            id
            name
            description
            executors
            dedline
            status
            column_id
            position
        }
    }
`;