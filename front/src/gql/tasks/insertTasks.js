import gql from 'graphql-tag';

export const INSERT_TASKS = gql`
    mutation insertTasks(
        # $id: Int,
        # $name: String,
        # $description: String,
        # $column_id: Int,
        # $dedline: Date,
        # $position: Int,
        # $status: String,
        # $executors: Int
        $array: [tasks_insert_input!]!
    ) {
        insert_tasks(objects: $array) {
            affected_rows
           
        }
    }
`;