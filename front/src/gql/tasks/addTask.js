import gql from 'graphql-tag';

export const ADD_TASK = gql`
    mutation addTask(
        $name: String!
        $description: String!
        $status: String!
        $priority: String!
        $executors: Int
        $dedline: date,
        $column_id: Int
    ) {
        insert_tasks(
            objects: [
                {
                    name: $name,
                    description: $description,
                    status: $status,
                    priority: $priority
                    executors: $executors
                    dedline: $dedline
                    column_id: $column_id
                }
            ]
        ) {
            returning {
                id
            }
        }
    }
`;