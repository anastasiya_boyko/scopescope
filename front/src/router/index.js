import Vue from 'vue';
import VueRouter from 'vue-router';
import Scope from '../pages/Scope.vue';
import Home from '../pages/Home.vue';
import BoardItem from '../pages/BoardItem';
import Login from '../pages/Login';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        props: true
    },
    {
        path: '/scope',
        name: 'Scope',
        component: Scope,
        props: true
    },
    {
        path: '/scope/board:id',
        name: 'BoardItem',
        component: BoardItem,
        props: true
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        props: true
    },
];

const router = new VueRouter ({
    mode: 'history',
    routes
});


export default router;