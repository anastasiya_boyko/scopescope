FROM node:10-alpine

RUN mkdir /usr/front
WORKDIR /usr/front
COPY package.json ./
RUN npm install
ENTRYPOINT npm run serve

EXPOSE 80
CMD [ "npm", "run", "serve" ]